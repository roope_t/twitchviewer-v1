<!DOCTYPE html>
<html class="overflow">
    <head>
        <meta charset="UTF-8">
        <!-- © Roope Tapaninen 2016 -->
        <meta name="author" content="Roope Tapaninen">
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel="stylesheet" type="text/css" href="./style/reset.css"/>
        <link rel="stylesheet" type="text/css" href="./style/style.css"/>
        <script type="text/javascript" src="./js/jquery-2.2.0.min.js"></script>
        <script type="text/javascript" src="./js/scripts.js"></script>
        <title></title>
    </head>
    <body id="site">
        <header id="header">
            <div class="title">
                <img class="titlebg" src='images/tlogo.png' alt=''>
            </div>
            <div class="form">
                <form id="form">
                    <label for="search">Search for a game or scroll down!</label>
                    <input type="text" maxlength="25" id="search" autofocus>
                    <input type="button" value="Search" id="btnSearch">
                </form>
            </div>
        </header>
        <div id="view">
        </div>
    </body>
</html>
