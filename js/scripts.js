/* © Roope Tapaninen 2016 */
$(window).on('beforeunload', function() {
    $(window).scrollTop(0);
});

$(document).ready(function(){
    $('#form').submit(function() {
        return false;
    });
    
    $("#btnSearch").click(function() {
        $(window).scrollTop(0);
        window.TwitchViewer.getGames();
    }); // .bind(this)
    
    $("#search").keypress(function(event) {
        if (event.which === 13)
        {
            $("#btnSearch").click();
        }
    });
    $(window).scroll(function(){
        window.TwitchViewer.navAnimations();
    });
    $(window).on("scroll", function() {
        $("#header").slideDown(500);
    }, 750); /*Happens after 750ms not scrolling*/
    
    
    window.TwitchViewer.initialize();
    
});

window.TwitchViewer =
{
    imgError : undefined,
    searhcUrl : undefined,
    nextContent : undefined,
    scrollForMore : undefined,
    searchTopGames : undefined,
    lastScrollTop : undefined,
    st : undefined,
    scrollForStreams : undefined,
    searchTopStreams : undefined,
    nextStreams : undefined,
    
    initialize : function() {
        this.imgError = 'this.onerror=null;this.src="http://static-cdn.jtvnw.net/ttv-static/404_boxart-272x380.jpg"';
        this.scrollForMore = false;
        this.scrollForStreams = false;
        this.getGamesTop();
    },
    
    trueOrFalse : function() {
        if(this.scrollForMore) {
            window.TwitchViewer.getGamesTop();
        }
    },
    trueOrFalseStreams : function() {
        if(this.scrollForStreams) {
            window.TwitchViewer.getChannels()
        }
    },
    
    navAnimations : function() {
        if($(window).scrollTop() + $(window).height() > $(document).height() - 100) {
            this.trueOrFalse();
            this.trueOrFalseStreams();
        }
        this.st = $(window).scrollTop();
        if(this.st > this.lastScrollTop) {
            $("#header").slideUp(500);
        }
        this.lastScrollTop = this.st; 
    },
    
    getGames : function() {
        this.scrollForMore = false;
        this.scrollForStreams = false;
        var search = $("#search").val();

        if (search.length > 0) {
            $("#view").empty();
            this.searchUrl = 'https://api.twitch.tv/kraken/search/games?q=' + search + '&type=suggest&live=true';
            $.ajax({
                url: this.searchUrl,
                dataType: 'json',
                type: 'get',
                cache: false,
                success: function(data) {
                    $(data.games).each(function(index, value){
                        var name = value.name;
                        var append = $("<div class='listitem' id='result-" + index 
                            + "' data-link='" + name + "'>"
                            + "<div class='listitem-container'>"
                            + "<div class='listitem-header'><p>" + name + "</p></div>"
                            + "<div class='listitem-main-80'>"
                            + "<img class='image' src='" + value.box.large
                            + "' alt='img" + index + "'/></div>"
                            + "</div></div>");
                    
                        append.click(function() {
                            window.TwitchViewer.getChannels($(this));
                        });
                    
                        $("#view").append(append);
                        $("#result-" + index).hide();
                        if (name.length>30) {
                            $("#result-" + index + " .listitem-header").css({"font-size": "1.2em"});
                        }
                        $("#result-" + index).fadeIn(1000);
                    });
                }
            });
        } else {
            window.TwitchViewer.getGamesTop();
        }
    },
    
    getLink : function(link) {
        this.nextContent = link;
    },
    getStreamLink : function(link) {
        this.nextStreams = link;
    },
    
    getGamesTop : function() {
        this.scrollForStreams = false;
        if (this.scrollForMore === false) {
            $("#view").empty();
            this.searchTopGames = "https://api.twitch.tv/kraken/games/top";
        } else {
            this.searchTopGames = this.nextContent;
        }
        this.scrollForMore = true;
        $.ajax({
                url: this.searchTopGames,
                dataType: 'json',
                type: 'get',
                cache: false,
                success: function(data) {
                    var nextLink = data._links.next;
                    $(data.top).each(function(index, value){
                        var name = value.game.name;
                        if (!($("#result-" + value.game._id).length)) {
                            var append = $("<div class='listitem' id='result-" + value.game._id 
                                + "' data-link='" + name + "'>"
                                + "<div class='listitem-container'>"
                                + "<div class='listitem-header'><p>" + name + "</p></div>"
                                + "<div class='listitem-main'>"
                                + "<img class='image' src='" + value.game.box.large
                                + "' alt=''/></div>"
                                + "<div class='listitem-footer'>"
                                + "<p>Viewers: " + value.viewers + "</p>" 
                                + "<p>Channels: " + value.channels + "</p>"
                                + "</div></div>");

                            append.click(function() {
                                $(window).scrollTop(0);
                                window.TwitchViewer.getChannels($(this));
                            });

                            $("#view").append(append);
                            $("#result-" + value.game._id).hide();
                            if (name.length>30) {
                                $("#result-" + index + " .listitem-header").css({"font-size": "1.2em"});
                            }
                            $("#result-" + value.game._id).fadeIn(1000);
                        }
                    });
                    window.TwitchViewer.getLink(nextLink);
                }
            });
    },
    
    getChannels : function(jqueryElem) {
        this.scrollForMore = false;
        if (this.scrollForStreams === false) {
            var gameName = jqueryElem.attr('data-link');
            this.searchTopStreams = "https://api.twitch.tv/kraken/streams?game=" + gameName + "&limit=25&offset=0";
            $("#view").empty();
        } else {
            this.searchTopStreams = this.nextStreams;
        }
        
        this.scrollForStreams = true;
        $.ajax({
                url: this.searchTopStreams,
                dataType: 'json',
                type: 'get',
                cache: false,
                success: function(data) {
                    var nextLink = data._links.next;
                    $(data.streams).each(function(index, value){
                        var chanId = value.channel._id;
                        if (!($("#result-" + chanId).length)) {
                            var status = value.channel.status;
                            var name = value.channel.display_name;

                            if (status.length >= 50){
                                status = status.substring(0, 50) + "...";
                            }

                            var append = $("<div class='listitem' id='result-" + chanId 
                                + "' data-link='" + name + "'>"
                                + "<div class='listitem-container'>"
                                + "<div class='listitem-header'><p>" + name + "</p></div>"
                                + "<div class='listitem-main'>"
                                + "<img class='image' src='" + value.preview.medium
                                + "' alt='' onError='" + this.imgError + "'/></div>"
                                + "<div class='listitem-footer'>"
                                + "<p>Status: " + status + "</p>"
                                + "<p>Viewers: " + value.viewers + "</p>" 
                                + "</div></div>");

                            append.click(function() {
                                $(window).scrollTop(0);
                                window.TwitchViewer.getStream($(this));
                            });

                            $("#view").append(append);
                            $("#result-" + chanId).hide();
                            if (status.length>30) {
                                $("#result-" + chanId + " .listitem-header").css({"font-size": "1.2em"});
                            }
                            $("#result-" + chanId).fadeIn(1000);
                        }
                    });
                    window.TwitchViewer.getStreamLink(nextLink);
                }
        });
    },
    
    getStream : function(jqueryElem) {
        this.scrollForMore = false;
        this.scrollForStreams = false;
        $("html").removeClass("overflow");
        var streamerName = jqueryElem.attr('data-link');
        $("#view").empty();
        $("#view").addClass("video-and-chat");
        $("#header").css({"top":"-5em"});
        var returnbutton = $("<div id='return-button'><p>Return to other streams</p></div>");
        returnbutton.click(function () {
            $(window).scrollTop(0);
            $("html").addClass("overflow");
            $("#view").empty();
            $("#view").removeClass("video-and-chat");
            $("#header").css({"top":"0"});
            this.scrollForMore = true;
            window.TwitchViewer.getGamesTop();
        });
        $("#view").append(returnbutton);
        streamerName = streamerName.replace(/\s+/, "");
        var append = $('<iframe class="livestream" src="http://www.twitch.tv/' + streamerName + '/embed"' 
                + ' frameborder="0" scrolling="no" allowfullscreen="yes"></iframe><a href="http://www.twitch.tv/' + streamerName 
                + '?tt_medium=live_embed&tt_content=text_link" style="padding:2px 0px 4px;'
                + ' display:block; width:345px; font-weight:normal; font-size:10px;text-decoration:underline;">' 
                + '</a><iframe class="chat" src="http://www.twitch.tv/' + streamerName + '/chat?popout=" frameborder="0"' 
                + ' scrolling="no"></iframe>');
        $("#view").append(append);
    }
};