Published @ roope.tapaninen.org/portfolio/twitchviewer

Update 1.01 (16.2.2016): Search fixed. Update 1.02: "games with ' in their name" -bug fixed.



TwitchViewer V1. (14.2.2016)

Ps. this was made with a rush in 2 days. And I didn't really pay attention to commenting. :D

Known bugs:

Some streams/games will be missed when scrolling because a link gotten from previous ajax request doesn't have offset set properly (skips games/streams if their viewer count rises). However duplicates are filtered. This can be fixed with setting different offset on the link whilst still filtering duplicates.

Games with ' in their names cannot be searched for streams as their data-link attribute will be bugged.

Some issues on viewing with small screens. (Elements dislocated/too big) 

Will fix later.